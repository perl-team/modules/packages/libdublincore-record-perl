libdublincore-record-perl (0.03-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 21:16:45 +0100

libdublincore-record-perl (0.03-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 00:58:50 +0100

libdublincore-record-perl (0.03-3) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * Convert debian/copyright to machine-readable DEP5 format.

 -- Axel Beckert <abe@debian.org>  Sun, 07 Jun 2015 19:16:11 +0200

libdublincore-record-perl (0.03-2) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/watch: use dist-based URL.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * Remove obsolete comment from debian/watch
  * Switch to source format "3.0 (quilt)"
  * Fix the following lintian warnings:
    + package-has-a-duplicate-build-relation
    + copyright-with-old-dh-make-debian-copyright
    + copyright-refers-to-symlink-license
  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency accordingly
  * Revamp debian/rules:
    + Fix lintian warning debian-rules-missing-recommended-target
    + Replace "dh_clean -k" with "dh_prep"
    + Use dh_auto_{configure,build,test,install,clean}
    + Drop obsolete parameters from dh_{installchangelogs,clean}
    + Remove obsolete variables and targets
    + Finally switch to a minimal dh-style debian/rules file
  * No more install README as it's generated from POD
  * Bump Standards-Version to 3.9.5 (no further changes)

 -- Axel Beckert <abe@debian.org>  Tue, 24 Dec 2013 04:51:16 +0100

libdublincore-record-perl (0.03-1) unstable; urgency=low

  * Initial Release (ITP Closes: #452715)

 -- Vincent Danjean <vdanjean@debian.org>  Sat, 24 Nov 2007 17:19:50 +0100
